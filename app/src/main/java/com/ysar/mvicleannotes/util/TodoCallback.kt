package com.ysar.mvicleannotes.util

// simple callback to execute something after a function is called
interface TodoCallback {

    fun execute()
}