package com.ysar.mvicleannotes.framework.presentation

import com.ysar.mvicleannotes.business.domain.state.DialogInputCaptureCallback
import com.ysar.mvicleannotes.business.domain.state.Response
import com.ysar.mvicleannotes.business.domain.state.StateMessageCallback


interface UIController {

    fun displayProgressBar(isDisplayed: Boolean)

    fun hideSoftKeyboard()

    fun displayInputCaptureDialog(title: String, callback: DialogInputCaptureCallback)

    fun onResponseReceived(
        response: Response,
        stateMessageCallback: StateMessageCallback
    )

}