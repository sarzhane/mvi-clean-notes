package com.ysar.mvicleannotes.framework.presentation.notedetail.state

import android.os.Parcelable
import com.ysar.mvicleannotes.business.domain.model.Note
import com.ysar.mvicleannotes.business.domain.state.ViewState
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NoteDetailViewState(

    var note: Note? = null,

    var isUpdatePending: Boolean? = null

) : Parcelable, ViewState









