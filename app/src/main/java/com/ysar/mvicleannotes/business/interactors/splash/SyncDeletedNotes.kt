package com.ysar.mvicleannotes.business.interactors.splash

import com.ysar.mvicleannotes.business.data.cache.CacheResponseHandler
import com.ysar.mvicleannotes.business.data.cache.abstraction.NoteCacheDataSource
import com.ysar.mvicleannotes.business.data.network.ApiResponseHandler
import com.ysar.mvicleannotes.business.data.network.abstraction.NoteNetworkDataSource
import com.ysar.mvicleannotes.business.data.util.safeApiCall
import com.ysar.mvicleannotes.business.data.util.safeCacheCall
import com.ysar.mvicleannotes.business.domain.model.Note
import com.ysar.mvicleannotes.business.domain.state.DataState
import com.ysar.mvicleannotes.util.printLogD
import kotlinx.coroutines.Dispatchers

class SyncDeletedNotes(
    private val noteCacheDataSource: NoteCacheDataSource,
    private val noteNetworkDataSource: NoteNetworkDataSource
){

    suspend fun syncDeletedNotes(){

        val apiResult = safeApiCall(Dispatchers.IO){
            noteNetworkDataSource.getDeletedNotes()
        }
        val response = object: ApiResponseHandler<List<Note>, List<Note>>(
            response = apiResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<Note>): DataState<List<Note>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }

        val notes = response.getResult()?.data?: ArrayList()

        val cacheResult = safeCacheCall(Dispatchers.IO){
            noteCacheDataSource.deleteNotes(notes)
        }

        object: CacheResponseHandler<Int, Int>(
            response = cacheResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: Int): DataState<Int>? {
                printLogD("SyncNotes",
                    "num deleted notes: ${resultObj}")
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

    }


}