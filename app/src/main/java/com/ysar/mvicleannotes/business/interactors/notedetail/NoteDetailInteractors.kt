package com.ysar.mvicleannotes.business.interactors.notedetail

import com.ysar.mvicleannotes.business.interactors.common.DeleteNote
import com.ysar.mvicleannotes.framework.presentation.notedetail.state.NoteDetailViewState


// Use cases
class NoteDetailInteractors (
    val deleteNote: DeleteNote<NoteDetailViewState>,
    val updateNote: UpdateNote
)